; character
{:name str
 :level int

 :left-hand buff
 :right-hand buff
 :body buff

 :whatever buff}

; base-chars
#{:str :dex :con :int :wis :cha}
; skills
#{:acrobatics
  :animals
  :arcana
  :athletics
  :deception
  :history
  :insight
  :intimidation
  :investigation
  :medicine
  :nature
  :perception
  :performance
  :persuasion
  :religion
  :sleight-of-hand
  :stealth
  :survival}

; buff
{:hp int
 base-chars int
 
 :save base-chars
 :save+ {base-chars int}
 :armor :all | #{...}
 :shield :all | #{...}
 :weapons #{:simple :martial}
 :tools #{...}}
