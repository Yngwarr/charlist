(ns charlist.core (:gen-class))
(require '[clojure.set :refer [union]])
(require '[clojure.test :refer [function?]])

(defn skill-base [skill]
  (get {:acrobatics :dex
        :animals :wis
        :arcana :int
        :athletics :str
        :deception :cha
        :history :int
        :insight :wis
        :intimidation :cha
        :investigation :int
        :medicine :wis
        :nature :int
        :perception :wis
        :performance :cha
        :persuasion :cha
        :religion :int
        :sleight-of-hand :dex
        :stealth :dex
        :survival :wis} skill))

(defn score [query-result]
  "reduce query result"
  (->> query-result (map second) (reduce +)))

(defn eval-funs [character query-result]
  "evaluate possible functions in query result"
  (->> query-result
       (map (fn [[k v]] [k (if (function? v) (v character) v)]))
       (into {})))

(defn query [value character]
  "returns a hash-set of the value's sources and values"
  (->> character
      (filter #(and (map? (second %)) (get (second %) value)))
      (map #(vec [(first %) (get (second %) value)]))
      (eval-funs character)))

(defn query-score [value character]
  "for querying scores of base characteristics"
  (reduce + (map second (query value character))))

(defn query-mod [value character]
  "for querying modifiers"
  (-> (query-score value character)
      (- 10)
      (/ 2)
      (Math/floor)
      (int)))

(defn query-set [value character]
  "for querying sets of weapon proficiencies etc."
  (reduce union (map second (query value character))))

(defn prof
  ; calculate proficiency bonus from lvl
  ([lvl] (int (+ (/ (- lvl 1) 4) 2)))
  ; get a specific proficiency bonus if appliable (e.g. (prof :save :str urist))
  ([kind value character]
   (if (contains? (query-set kind character) value) (prof (get character :level)) 0)))

(defn query-save [value character]
  "query scores for savethrow bonuses"
  (merge
    (query value (query :save+ character))
    {:modifier (query-mod value character)
     :proficiency (prof :save value character)}))

(defn query-skill [value character]
  "query skill scores"
  (merge
    (query value (query :skills+ character))
    {:modifier (query-mod (skill-base value) character)
     :proficiency (prof :skills value character)}))

(defn query-ac [character]
  (if (empty? (query :ac character))
    (+ 10 (query-mod :dex character))
    (query :ac character)))

; constants
(def base-chars #{:str :dex :con :int :wis :cha})

; races and subraces
(def dwarf
  {:name "Dwarf"
   :con 2
   :speed 25
   :buffs #{:darkvision}})
(def mountain-dwarf {:subrace "Mountain Dwarf" :str 2})

; classes
(def fighter
  {:name "Fighter"
   :save #{:str :con}
   :armor :all
   :shield :all
   :weapons #{:simple :martial}
   :tools #{}})

;items
(def hammer-of-doom
  {:name "Hammer of Doom"
   :save+ {:dex 7}
   :skills+ {:insight 2}})

(def padded-armor
  {:name "Padded Armor"
   :ac #(+ 11 (query-mod :dex %))
   :disadvantage #{:stealth}
   :weight 8})

(def shield
  {:name "Shield"
   :ac 2
   :weight 6})

(def chain-mail
  {:name "Chain Mail"
   :ac 16
   :require #(> (query-score :str %) 13)
   :disadvantage #{:stealth}
   :weight 55})

; characters
(def urist
  {:name "Urist"
   :race dwarf
   :subrace mountain-dwarf
   :class fighter
   :level 7
   :initial {:hp 12 :str 15 :dex 14 :con 13 :int 12 :wis 10 :cha 8}
   :class-choice {:skills #{:insight :survival}}
   :left-hand hammer-of-doom
   :right-hand shield
   :body padded-armor
   :lvl2 {:hp 8}})

(def rhogar
  {:name "Rhogar"
   :body chain-mail})

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))
